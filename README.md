# "Building an Open Source Artificial Pancreas" (full talk)
# "I Built an Artificial Pancreas!" (lightning talk)

## Abstract
Have you ever thought about what open source software or hardware could achieve? What if it could help improve people's lives by solving some of their health problems?

After the medical tech industry kept promising a system to help automatically manage insulin for type 1 diabetic people and never delivering, some people got together to find ways to do it with the tech they already had. Over the past few years, a "closed-loop" system has been developed to algorithmically regulate people's blood sugars. After reverse engineering bluetooth sensors and 915 MHz insulin pumps, the system became possible. As a diabetic, I also built this system and saw my sugar values stabilize much more than I could ever achieve doing it manually myself. Now I'm working on contributing back to the projects as well.

I want to talk about this system, from a technical side as well as a personal side. I'll talk about OpenAPS (the open artificial pancreas system) and how it works, what problems it solves, and its safety and security concerns. I also want to show how it's helped me, and what this means for my health now and in the future. I ultimately want to show how we, as software developers, can change people's lives through the code we write.

## Version History:
Date      | Type           | Version | Venue
--------- | -------------- | ------- | ------
8/17/2018 | Lightning Talk | 1.0     | Self.Conference
9/27/2018 | Lightning Talk | 1.1     | Strange Loop
1/9/2019  | Lightning Talk | 1.2     | Code Mash
3/3/2019  | Conf Session   | 1.0     | Penguicon
3/4/2019  | Conf Session   | 1.1     | PyCon US
3/11/2019 | Lightning Talk | 1.3     | !!Con
6/?/2019  | Conf Session   | ?       | Self.Conference
8/?/2019  | Conf Session   | ?       | THAT Conference
11/?/2019 | Conf Session   | ?       | Agile + DevOps East

? means it's in the future or I'm uncertain of that information

## Contact
I'd love to answer any questions you have about my session. The easiest method to contact me is by Twitter: [@geekygirlsarah](https://www.twitter.com/geekygirlsarah). Other contact info is on my last slide.
